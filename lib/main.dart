// ignore_for_file: prefer_const_constructors, sort_child_properties_last, import_of_legacy_library_into_null_safe, prefer_is_not_empty, unnecessary_null_comparison, deprecated_member_use, depend_on_referenced_packages, avoid_function_literals_in_foreach_calls, prefer_final_fields, prefer_typing_uninitialized_variables, use_build_context_synchronously
import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:intl/intl.dart';
import 'package:tuple/tuple.dart';
import 'widgets/returnOrigin.dart';
import 'widgets/returnDestination.dart';
import 'package:geolocator/geolocator.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';
import 'package:weather_icons/weather_icons.dart';
import 'config/config.dart';
import 'package:latlong/latlong.dart' as latlng;

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter App',
      home: HomePage(),
      theme: ThemeData(
        fontFamily: 'Rubik',
        primaryColor: Color.fromRGBO(23, 26, 32, 1),
        accentColor: Color.fromRGBO(250, 250, 250, 1),
      ),
    );
  }
}

/// Helper class to create polylines.
class LineString {
  LineString(this.lineString);
  List<dynamic> lineString;
}

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  Config config = Config();

  /// Initial map coordinates and map controller to control camera movements.
  late final MapController _mapController = MapController();

  /// Input field texts for origin/dest points.
  String originText = 'Starting Point...';
  String destinationText = 'Destination...';

  /// Variable related to origin/destination coordinates, markers, shortest path coordinates, corresponding polyline list,
  /// total duration, distance and time period.
  late latlng.LatLng _originCoordinates;
  late latlng.LatLng _destCoordinates;
  int _markerCounter = 0;
  Set<Marker> _markers = {};
  var _data;
  List<latlng.LatLng> _polyline = <latlng.LatLng>[];
  String _totalDuration = '';
  String _totalDistance = '';
  String _totalDaysFormatted = '';

  /// Variables related to filters such as weather and date.
  DateTimeRange _dateRange = DateTimeRange(
    start: DateTime.now(),
    end: DateTime.now(),
  );
  bool _isDateChosen = false;
  bool _showAdditionalButtons = false;
  List<String> _selectedOptions = [];
  var _dateRangeArray = [];
  var _availableDatesForTrip = [];

  /// Variables related to weather API.
  var _forecastList = [];
  bool _isCollapsed = false;
  bool _isLoading = false;

  /// Takes the [inputText] value of [String] type and returns a formatted version of it.
  String _getFormattedText(String inputText) {
    if (inputText != null) {
      if (inputText.length > 15) {
        return '${inputText.substring(0, 15)}...';
      }
    }
    return inputText;
  }

  /// Takes [context] value of [BuildContext] type and awaits address information of [String] and checks if there is
  /// a corresponding address. If there is corresponding address, updates origin text and origin coordinates for given
  /// address.
  void _awaitStartingPointReturnValue(BuildContext context) async {
    final result = await Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => ReturnOrigin(originText),
      ),
    );
    if (result != null) {
      //Match adress with Backend
      final response = await http.post(
          Uri.parse('http://10.0.2.2:8080/addressmatching'),
          body: jsonEncode({"address": result}),
          headers: <String, String>{"Content-Type": "application/json"});
      _data = jsonDecode(response.body);
      var coordinate = _data["coordinate"];

      if (_data.containsKey('error_code') && _data['error_code'] != 0) {
        switch (_data['error_code']) {
          case 1:
            _showMessage(
                'The address is outside the solution space. Please enter an address within Berlin-Brandenburg.',
                context);
            return;
          case 2:
            _showMessage('The address could not be found.', context);
            return;
        }
      }

      setState(() {
        originText = result;
        _originCoordinates =
            latlng.LatLng(coordinate["lat"], coordinate["lon"]);
      });
    }
    // Toggle keyboard
    FocusManager.instance.primaryFocus?.unfocus();
    // Animate camera to the starting point.
    _mapController.move(_originCoordinates, 13);
    setState(() {
      _markers = {};
    });
    _addOriginMarker();
    _resetPolyline();
  }

  /// Takes [context] value of [BuildContext] type and awaits address information of [String] and checks if there is
  /// a corresponding address. If there is corresponding address, updates destination text and destination coordinates
  /// for given address.
  void _awaitDestinationPointReturnValue(BuildContext context) async {
    final result = await Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => ReturnDestination(destinationText),
      ),
    );
    if (result != null) {
      //Match adress with Backend
      final response = await http.post(
          Uri.parse('http://10.0.2.2:8080/addressmatching'),
          body: jsonEncode({"address": result}),
          headers: <String, String>{"Content-Type": "application/json"});
      _data = jsonDecode(response.body);
      var coordinate = _data["coordinate"];

      if (_data.containsKey('error_code') && _data['error_code'] != 0) {
        switch (_data['error_code']) {
          case 1:
            _showMessage(
                'The address is outside the solution space. Please enter an address within Berlin-Brandenburg.',
                context);
            return;
          case 2:
            _showMessage('The address could not be found.', context);
            return;
        }
      }
      setState(
        () {
          destinationText = result;
          _destCoordinates =
              latlng.LatLng(coordinate["lat"], coordinate["lon"]);
        },
      );
    }
    // Toggle keyboard
    FocusManager.instance.primaryFocus?.unfocus();
    // Animate camera to the destionation point.
    _mapController.move(_destCoordinates, 13);
    if (_markers.length > 1) {
      _markers.remove(_markers.last);
    }
    setState(() {
      _markers;
    });
    _addDestinationMarker();
    _resetPolyline();
  }

  /// Reset already existing polylines and call [_getDirection()] function along with [_getWeather()] function.
  void _getShortestPath() async {
    _resetPolyline();

    _isLoading = true;

    await Future.wait<void>([
      _getWeather(),
      _getDirections(),
    ]);

    _isLoading = false;
    setState(() {
      _isLoading;
    });
  }

  /// Creates Marker instance based on origin coordinates and updates [_marker] list.
  void _addOriginMarker() {
    _markers.add(
      Marker(
        width: 140,
        height: 140,
        point: _originCoordinates,
        builder: (ctx) => Icon(
          Icons.location_on,
          color: Color.fromRGBO(0, 128, 0, 1),
        ),
      ),
    );
    setState(() {
      _markers;
      _markerCounter = 1;
    });
  }

  /// Creates Marker instance based on destination coordinates and updates [_marker] list.
  void _addDestinationMarker() {
    _markers.add(
      Marker(
        width: 100,
        height: 100,
        point: _destCoordinates,
        builder: (ctx) => Icon(
          Icons.location_on,
          color: Colors.red,
        ),
      ),
    );
    setState(() {
      _markers;
      _markerCounter = 0;
    });
  }

  /// Takes string of type [String] and context of type [BuildContext] to show alert dialog on screen
  void _showMessage(String string, BuildContext context) {
    showDialog<String>(
      context: context,
      builder: (BuildContext context) => AlertDialog(
        content: Text(string),
        actions: <Widget>[
          TextButton(
            onPressed: () => Navigator.pop(context, 'OK'),
            child: const Text('OK'),
          ),
        ],
      ),
    );
  }

  /// Makes post request to get coordinates of shortest path between origin and destination point. After necessary
  /// information fetched, updates [_data] variable to store coordinates. Then validates and checks whether the route was
  /// found. Updates [duration], [distance] and [daysAfterToday] variables and apply necessary formats. Then calls [drawPolyline]
  /// function to draw a polyline for given points, calculates bound and animates the camera depending on bounds.
  Future<void> _getDirections() async {
    _resetPolyline();

    try {
      var originlat = _originCoordinates.latitude;
      var originlon = _originCoordinates.longitude;
      var destlat = _destCoordinates.latitude;
      var destlon = _destCoordinates.longitude;
      var dateTup = getDayOffset();

      // Get direction data
      // No weather or days included for now
      final response = await http.post(Uri.parse('http://10.0.2.2:8080/route'),
          body: jsonEncode({
            "start_node": {"lat": originlat, "lon": originlon},
            "end_node": {"lat": destlat, "lon": destlon},
            "forbidden": getForbidden(),
            "start_after_today": dateTup.item1,
            "end_after_today": dateTup.item2
          }),
          headers: <String, String>{"Content-Type": "application/json"});
      _data = jsonDecode(response.body);

      if (_data.containsKey('error_code') && _data['error_code'] != 0) {
        switch (_data['error_code']) {
          case 1:
            _showMessage('No route was found.', context);
            return;
        }
      }

      var coordinates = _data['route'];
      int duration = _data['duration'];
      int distance = _data['distance'];
      int daysAfterToday = _data['days_after_today'];

      // Format duration, distance and day
      _totalDuration = getTotalDuration(duration);
      _totalDistance = getTotalDistance(distance);
      _totalDaysFormatted = getDay(daysAfterToday);

      // Draw polyline
      drawPolyline(coordinates);

      // Find bound and move camera
      final bounds = LatLngBounds.fromPoints([
        _originCoordinates,
        _destCoordinates,
      ]);
      _mapController.fitBounds(
        bounds,
        options: const FitBoundsOptions(
          padding: EdgeInsets.only(left: 30, right: 30),
        ),
      );

      setState(() {
        _totalDuration;
        _totalDistance;
        _totalDaysFormatted;
        _polyline;
      });
    } catch (e) {
      print(e);
    }
  }

  /// Takes [coordinates] of [List<LatLng>] type and add [_polyline] variable to store necessary polyline coordinates
  /// to render them on screen.
  void drawPolyline(coordinates) {
    LineString ls = LineString(coordinates);

    for (int i = 0; i < ls.lineString.length; i++) {
      _polyline.add(latlng.LatLng(ls.lineString[i][0], ls.lineString[i][1]));
    }
  }

  /// Gets [durationInMins] of [int] type and apply formatting to return [String].
  String getTotalDuration(int durationInMins) {
    int hours = (durationInMins / 60).floor();
    int min = (durationInMins % 60).floor();
    if (hours != 0) {
      return '$hours hours $min mins';
    }
    return '$min mins';
  }

  /// Gets [distance] of [int] type and apply formatting to return [String].
  String getTotalDistance(int distance) {
    int distanceInKm = (distance / 1000).ceil();
    return '$distanceInKm km';
  }

  /// Gets [daysAfterToday] of [int] type and apply formatting to return [String].
  String getDay(int daysAfterToday) {
    DateTime today = DateTime.now();
    String date = _convertDateFormat(today.add(Duration(days: daysAfterToday)));
    print(date);
    return date;
  }

  /// Updates [forbidden] of [List<String>] type based on undesired weather options and returns it.
  List<String> getForbidden() {
    List<String> forbidden = [];
    for (var option in _selectedOptions) {
      switch (option) {
        case 'Rain':
          forbidden.add('rain');
          break;
        case 'Frost':
          forbidden.add('ice');
          break;
        case 'Wind':
          forbidden.add('wind');
          break;
        case 'Snow':
          forbidden.add('snow');
      }
    }

    return forbidden;
  }

  /// Returns tuple of [int] based on desired date range entered by user.
  Tuple2<int, int> getDayOffset() {
    return Tuple2<int, int>(_dateRange.start.difference(DateTime.now()).inDays,
        _dateRange.end.difference(DateTime.now()).inDays);
  }

  /// Helper function to reset polyline before calculating new polyline for another route.
  void _resetPolyline() {
    _polyline = [];
    setState(() {
      _polyline;
    });
  }

  /// Helper function to reset marker.
  void _resetMarker() {
    _markers = {};
    setState(() {
      _markers;
    });
  }

  /// Based on user's current position, makes http request with the help of users latitude and longitude information,
  /// updates [originText] and [_originCoordinates] based on these information and locates user with the help of geolocation.
  void _getCurrentLocation() async {
    await Geolocator.requestPermission().then(
      (value) => {
        Geolocator.getCurrentPosition(
                desiredAccuracy: LocationAccuracy.best,
                forceAndroidLocationManager: true)
            .then(
          (Position position) async {
            final url = Uri.parse(
                'https://maps.googleapis.com/maps/api/geocode/json?latlng=${position.latitude},${position.longitude}&key=${config.GOOGLE_API}');
            final response = await http.get(url);
            setState(() {
              originText =
                  json.decode(response.body)['results'][0]['formatted_address'];
              _originCoordinates =
                  latlng.LatLng(position.latitude, position.longitude);
            });
            _mapController.move(_originCoordinates, 13);
            _resetMarker();
            _addOriginMarker();
            _resetPolyline();
          },
        ).catchError(
          (e) {
            print(e);
          },
        ),
      },
    );
  }

  /// Creates Marker instance based on users touch and updates [_marker] list.
  void _appearOriginMarkerOnTouch(latlng.LatLng pos) async {
    _resetPolyline();
    _resetMarker();
    _markers.add(
      Marker(
        width: 100,
        height: 100,
        point: pos,
        builder: (ctx) => Icon(
          Icons.location_on,
          color: Color.fromRGBO(0, 128, 0, 1),
        ),
      ),
    );
    final url = Uri.parse(
        'https://maps.googleapis.com/maps/api/geocode/json?latlng=${pos.latitude},${pos.longitude}&key=${config.GOOGLE_API}');
    final response = await http.get(url);
    originText = json.decode(response.body)['results'][0]['formatted_address'];
    setState(() {
      _markers;
      originText;
      _originCoordinates = latlng.LatLng(pos.latitude, pos.longitude);
      _markerCounter = 1;
    });
  }

  /// Creates Marker instance based on users touch and updates [_marker] list.
  void _appearDestMarkerOnTouch(latlng.LatLng pos) async {
    _resetPolyline();
    if (_markers.length > 1) {
      _markers.remove(_markers.elementAt(1));
    }
    _markers.add(
      Marker(
        width: 100,
        height: 100,
        point: pos,
        builder: (ctx) => Icon(
          Icons.location_on,
          color: Colors.red,
        ),
      ),
    );
    final url = Uri.parse(
        'https://maps.googleapis.com/maps/api/geocode/json?latlng=${pos.latitude},${pos.longitude}&key=${config.GOOGLE_API}');
    final response = await http.get(url);
    destinationText =
        json.decode(response.body)['results'][0]['formatted_address'];
    setState(() {
      _markers;
      destinationText;
      _destCoordinates = latlng.LatLng(pos.latitude, pos.longitude);
      _markerCounter = 0;
    });
  }

  /// First navigates user to a different page and let user to choose a time period. After uses choses a time period, then
  /// updates_dateRange instance of type [DateTimeRange]. Takes [ctx] of [BuildContext] type.
  _rangeDatePicker(BuildContext ctx) async {
    DateTimeRange? newDateTimeRange = await showDateRangePicker(
      initialEntryMode: DatePickerEntryMode.calendarOnly,
      context: context,
      firstDate: DateTime.now(),
      lastDate: DateTime(DateTime.now().year + 2),
      initialDateRange: _dateRange,
      builder: (context, child) {
        return Theme(
          data: Theme.of(context).copyWith(
            colorScheme: ColorScheme.light(
              primary: Theme.of(context).primaryColor,
              onPrimary: Theme.of(context).accentColor,
              onSurface: Theme.of(context).accentColor,
            ),
            textButtonTheme: TextButtonThemeData(
              style: TextButton.styleFrom(
                primary: Theme.of(context).accentColor, // Button text color
              ),
            ),
          ),
          child: child!,
        );
      },
    );

    // Update chosen date of global date variable.
    if (newDateTimeRange == null) return;
    setState(() {
      _dateRange = newDateTimeRange;
      _isDateChosen = true;
    });
    _updateDateRange();
  }

  /// Formats [_dateRange] variable and store start and end dates in a seperate list called [_dateRangeArray].
  void _updateDateRange() {
    DateFormat formatter = DateFormat('y-MM-dd');
    String formattedStart = formatter.format(_dateRange.start);
    String formattedEnd = formatter.format(_dateRange.end);
    _dateRangeArray.add(formattedStart);
    _dateRangeArray.add(formattedEnd);
  }

  /// Helper function that takes [date] as argument and returns formatted [String].
  String _convertDateFormat(date) {
    DateFormat formatter = DateFormat('MM/dd');
    String formatted = formatter.format(date);
    return formatted;
  }

  /// Helper function to display date.
  String _displayDate() {
    if (_isDateChosen) {
      return "${_convertDateFormat(_dateRange.start)} - ${_convertDateFormat(_dateRange.end)}";
    }
    return '';
  }

  /// Helper fnuction to toggle additional button for filter such as weather and calendar.
  void _toggleAdditionalButtons() {
    setState(() {
      _showAdditionalButtons = !_showAdditionalButtons;
    });
  }

  /// Display bottom sheet that contains weather options for client to choose undesired weather conditions.
  void _showOptions(BuildContext context) {
    showModalBottomSheet(
      context: context,
      builder: (BuildContext context) {
        return Container(
          child: Wrap(
            children: [
              ListTile(
                leading: Icon(WeatherIcons.rain),
                title: Text('Rain'),
                onTap: () {
                  setState(() {
                    if (!_selectedOptions.contains('Rain')) {
                      _selectedOptions.add('Rain');
                    }
                  });
                  Navigator.of(context).pop();
                },
              ),
              ListTile(
                leading: Icon(WeatherIcons.wind_beaufort_1),
                title: Text('Wind'),
                onTap: () {
                  setState(() {
                    if (!_selectedOptions.contains('Wind')) {
                      _selectedOptions.add('Wind');
                    }
                  });
                  Navigator.of(context).pop();
                },
              ),
              ListTile(
                leading: Icon(WeatherIcons.snow),
                title: Text('Snow'),
                onTap: () {
                  setState(() {
                    if (!_selectedOptions.contains('Snow')) {
                      _selectedOptions.add('Snow');
                    }
                  });
                  Navigator.of(context).pop();
                },
              ),
              ListTile(
                leading: Icon(WeatherIcons.fog),
                title: Text('Frost'),
                onTap: () {
                  setState(() {
                    if (!_selectedOptions.contains('Frost')) {
                      _selectedOptions.add('Frost');
                    }
                  });
                  Navigator.of(context).pop();
                },
              ),
            ],
          ),
        );
      },
    );
  }

  /// Helper function to create a list of widgets that contains [Text] widget and [Icon] widget together for undesired weather.
  List<Widget> _returnWidget() {
    List<Widget> widgetList = [];
    for (var item in _selectedOptions) {
      widgetList.add(Text(
        item,
        style: TextStyle(
            letterSpacing: 0.6,
            fontSize: 19,
            fontWeight: FontWeight.w400,
            color: Theme.of(context).accentColor),
      ));
      widgetList.add(SizedBox(
        child: Text('  '),
      ));
      widgetList.add(
        Container(margin: EdgeInsets.only(bottom: 10), child: _getIcon(item)),
      );
      widgetList.add(
        Text(
          ' ,  ',
          style: TextStyle(color: Theme.of(context).accentColor, fontSize: 26),
        ),
      );
    }
    return widgetList.sublist(0, widgetList.length - 1);
  }

  /// Helper function to get corresponding icon. Takes [iconName] of [String] type and return corresponding [Icon].
  Icon _getIcon(String iconName) {
    switch (iconName) {
      case 'Rain':
        return Icon(
          WeatherIcons.rain,
          color: Theme.of(context).accentColor,
          size: 22,
        );
      case 'Wind':
        return Icon(
          WeatherIcons.wind_beaufort_1,
          color: Theme.of(context).accentColor,
          size: 22,
        );
      case 'Snow':
        return Icon(
          WeatherIcons.snow,
          color: Theme.of(context).accentColor,
          size: 22,
        );
      case 'Frost':
        return Icon(
          WeatherIcons.fog,
          color: Theme.of(context).accentColor,
          size: 22,
        );
      default:
        return Icon(Icons.abc);
    }
  }

  /// Creates weather query to the external weather API and checks weather conditions in destination point. If weather
  /// conditions in a particular date contradicts with the undesired weather conditions of user, then exlude that day. Otherwise
  /// add that day to a [_availableDatesForTrip] list and update it at the end.
  Future<void> _getWeather() async {
    _forecastList = [];
    setState(() {
      _forecastList;
    });
    final response = await http.get(
      Uri.parse(
        'http://api.weatherapi.com/v1/forecast.json?key=${config.WEATHER_API}&q=${_destCoordinates.latitude},${_destCoordinates.longitude}&days=14',
      ),
    );

    // Get weather information for destination point and update list for available dates of trip.
    var data = jsonDecode(response.body);
    var forecast = data['forecast']['forecastday'];
    forecast.forEach(
      (item) => {
        _forecastList.add(
          {
            'date': item['date'],
            'chanceOfRain': item['day']['daily_chance_of_rain'],
            'chanceOfSnow': item['day']['daily_chance_of_snow'],
            'condition': item['day']['condition']['text'],
          },
        )
      },
    );
    _getAvailableDates();
  }

  void _getAvailableDates() {
    _availableDatesForTrip = [];
    setState(() {
      _availableDatesForTrip;
    });
    var format = DateFormat('y-dd-mm');
    var sinceEpochStart =
        format.parse(_dateRangeArray[0], true).millisecondsSinceEpoch;
    var sinceEpochEnd =
        format.parse(_dateRangeArray[1], true).millisecondsSinceEpoch;
    _forecastList.forEach((element) {
      final dt = format.parse(element['date'], true).millisecondsSinceEpoch;
      if (sinceEpochStart < dt && dt < sinceEpochEnd) {
        if (_selectedOptions.contains('Rain') &&
            _selectedOptions.contains('Snow')) {
          if ((element['chanceOfRain'].toInt() < 87) &&
              (element['chanceOfSnow'].toInt() < 85)) {
            _availableDatesForTrip.add(element['date']);
          }
        } else if (_selectedOptions.contains('Rain')) {
          if (element['chanceOfRain'].toInt() < 87) {
            _availableDatesForTrip.add(element['date']);
          }
        } else if (_selectedOptions.contains('Snow')) {
          if (element['chanceOfSnow'].toInt() < 85) {
            _availableDatesForTrip.add(element['date']);
          }
        } else {
          _availableDatesForTrip.add(element['date']);
        }
      }
      setState(() {
        _availableDatesForTrip;
      });
    });
  }

  /// Helper function to check if info button should be collapsed or not.
  void _collapse() {
    if (_isCollapsed) {
      _isCollapsed = false;
    } else {
      _isCollapsed = true;
    }
    setState(() {
      _isCollapsed;
    });
  }

  // Main App
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          FlutterMap(
            mapController: _mapController,
            options: MapOptions(
              center: latlng.LatLng(52.5163, 13.3777),
              zoom: 12,
              onTap: _markerCounter == 0
                  ? _appearOriginMarkerOnTouch
                  : _appearDestMarkerOnTouch,
            ),
            layers: [
              TileLayerOptions(
                urlTemplate:
                    'https://{s}.tile.openstreetmap.fr/hot/{z}/{x}/{y}.png',
                subdomains: ['a', 'b', 'c'],
              ),
              MarkerLayerOptions(
                markers: [
                  ..._markers,
                ],
              ),
              PolylineLayerOptions(
                polylines: [
                  Polyline(
                    points: _polyline,
                    color: Color.fromRGBO(9, 89, 95, 1), // Set the line's color
                    strokeWidth: 5.0, // Set the line's width
                  ),
                ],
              )
            ],
          ),
          SafeArea(
            child: Column(
              children: [
                // Starting Input
                Container(
                  margin:
                      EdgeInsets.only(top: 10, right: 20, bottom: 10, left: 20),
                  decoration: BoxDecoration(
                    border: Border.all(
                      width: 0.7,
                      color: Color.fromRGBO(0, 0, 0, 0.4),
                    ),
                    color: Theme.of(context).accentColor,
                    borderRadius: BorderRadius.all(
                      Radius.circular(30),
                    ),
                  ),
                  child: Row(
                    children: [
                      SizedBox(
                        height: 40,
                        child: Container(
                          padding: EdgeInsets.only(right: 15, left: 10),
                          child: Icon(
                            Icons.person_pin_circle_outlined,
                            size: 32,
                            color: Theme.of(context).primaryColor,
                          ),
                        ),
                      ),
                      Expanded(
                        child: SizedBox(
                          height: 40,
                          child: TextField(
                            decoration: InputDecoration(
                              labelStyle: TextStyle(
                                letterSpacing: 1,
                                color: Color.fromRGBO(20, 20, 20, 1),
                              ),
                              border: InputBorder.none,
                              prefixText: _getFormattedText(originText),
                              labelText: _getFormattedText(originText),
                              floatingLabelBehavior:
                                  FloatingLabelBehavior.never,
                            ),
                            style: TextStyle(
                              fontSize: 20,
                            ),
                            onTap: () {
                              _awaitStartingPointReturnValue(context);
                            },
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 40,
                        child: Container(
                          padding: EdgeInsets.only(right: 6, left: 10),
                          child: IconButton(
                            iconSize: 24,
                            icon: Icon(
                              Icons.my_location_outlined,
                              color: Theme.of(context).primaryColor,
                            ),
                            onPressed: _getCurrentLocation,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                // Destination Input
                Container(
                  margin: EdgeInsets.only(right: 20, bottom: 15, left: 20),
                  decoration: BoxDecoration(
                    border: Border.all(
                      width: 0.7,
                      color: Color.fromRGBO(0, 0, 0, 0.4),
                    ),
                    color: Theme.of(context).accentColor,
                    borderRadius: BorderRadius.all(
                      Radius.circular(30),
                    ),
                  ),
                  child: Row(
                    children: [
                      SizedBox(
                        height: 40,
                        child: Container(
                          padding: EdgeInsets.only(right: 15, left: 10),
                          child: Icon(
                            Icons.pin_drop_outlined,
                            size: 32,
                            color: Colors.red,
                          ),
                        ),
                      ),
                      Expanded(
                        child: SizedBox(
                          height: 40,
                          child: TextField(
                            decoration: InputDecoration(
                              labelStyle: TextStyle(
                                letterSpacing: 1,
                                color: Color.fromRGBO(20, 20, 20, 1),
                              ),
                              border: InputBorder.none,
                              prefixText: _getFormattedText(destinationText),
                              labelText: _getFormattedText(destinationText),
                              floatingLabelBehavior:
                                  FloatingLabelBehavior.never,
                            ),
                            style: TextStyle(
                              fontSize: 20,
                            ),
                            onTap: () {
                              _awaitDestinationPointReturnValue(context);
                            },
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Row(
                  children: [
                    Expanded(
                      child: SizedBox(
                        child: Container(
                          margin: EdgeInsets.only(left: 25),
                          child: Column(
                            children: [
                              Row(
                                children: [
                                  CircleAvatar(
                                    radius: 23,
                                    backgroundColor:
                                        Color.fromRGBO(9, 89, 95, 1),
                                    child: IconButton(
                                      onPressed: _toggleAdditionalButtons,
                                      icon: Icon(Icons.menu),
                                      color: Theme.of(context).accentColor,
                                    ),
                                  ),
                                  SizedBox(width: 10),
                                  if (_showAdditionalButtons)
                                    Expanded(
                                      child: Row(
                                        children: [
                                          CircleAvatar(
                                            backgroundColor:
                                                Color.fromRGBO(9, 89, 95, 1),
                                            radius: 20,
                                            child: IconButton(
                                              onPressed: () {
                                                _rangeDatePicker(context);
                                                // Do something when the first additional button is pressed
                                              },
                                              icon: Icon(Icons.calendar_month),
                                              color:
                                                  Theme.of(context).accentColor,
                                            ),
                                          ),
                                          SizedBox(width: 10),
                                          CircleAvatar(
                                            backgroundColor:
                                                Color.fromRGBO(9, 89, 95, 1),
                                            radius: 20,
                                            child: IconButton(
                                              onPressed: () {
                                                _showOptions(context);
                                                // Do something when the second additional button is pressed
                                              },
                                              icon: Icon(Icons.sunny),
                                              color:
                                                  Theme.of(context).accentColor,
                                            ),
                                          ),
                                          SizedBox(width: 10),
                                        ],
                                      ),
                                    ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    Container(
                      child: RawMaterialButton(
                        onPressed: _getShortestPath,
                        elevation: 5,
                        fillColor: Color.fromRGBO(9, 89, 95, 1),
                        child: Icon(
                          Icons.navigation_rounded,
                          color: Theme.of(context).accentColor,
                          size: 24,
                        ),
                        padding: EdgeInsets.all(12),
                        shape: CircleBorder(),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
          DraggableScrollableSheet(
            initialChildSize: 0.1,
            minChildSize: 0.1,
            maxChildSize: 0.2,
            builder: (BuildContext context, ScrollController scrollController) {
              return Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                    topRight: Radius.circular(20),
                    topLeft: Radius.circular(20),
                  ),
                  color: Theme.of(context).primaryColor,
                ),
                child: ListView.builder(
                  controller: scrollController,
                  itemCount: 1,
                  padding: EdgeInsets.all(20),
                  itemBuilder: (BuildContext context, int index) {
                    return Stack(
                      children: [
                        _isLoading
                            ? Center(child: const CircularProgressIndicator())
                            : Column(
                                children: [
                                  Container(
                                    margin: EdgeInsets.only(bottom: 15, top: 5),
                                    decoration: BoxDecoration(
                                        color: Color.fromRGBO(53, 56, 63, 1),
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(25))),
                                    padding: EdgeInsets.only(
                                        bottom: 15, top: 15, left: 20),
                                    child: Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Container(
                                          padding:
                                              EdgeInsets.only(right: 5, top: 2),
                                          child: Icon(
                                            Icons.calendar_month,
                                            color:
                                                Theme.of(context).accentColor,
                                            size: 20,
                                          ),
                                        ),
                                        Text(
                                          'Picked Date:  ',
                                          style: TextStyle(
                                              letterSpacing: 0.6,
                                              fontSize: 21,
                                              fontWeight: FontWeight.w500,
                                              color: Theme.of(context)
                                                  .accentColor),
                                        ),
                                        Text(
                                          _displayDate(),
                                          style: TextStyle(
                                              fontSize: 20,
                                              fontWeight: FontWeight.w400,
                                              color: Theme.of(context)
                                                  .accentColor),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(bottom: 10),
                                    padding: EdgeInsets.only(bottom: 15),
                                    decoration: BoxDecoration(
                                        color: Color.fromRGBO(53, 56, 63, 1),
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(25))),
                                    child: Column(
                                      children: [
                                        Container(
                                          padding: EdgeInsets.only(
                                              bottom: 5, top: 15, left: 17),
                                          child: Row(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Container(
                                                padding:
                                                    EdgeInsets.only(right: 8),
                                                child: Icon(
                                                  WeatherIcons.cloudy,
                                                  color: Theme.of(context)
                                                      .accentColor,
                                                  size: 18,
                                                ),
                                              ),
                                              Text(
                                                'Undesired Weather:  ',
                                                style: TextStyle(
                                                    letterSpacing: 0.6,
                                                    fontSize: 21,
                                                    fontWeight: FontWeight.w500,
                                                    color: Theme.of(context)
                                                        .accentColor),
                                              ),
                                            ],
                                          ),
                                        ),
                                        Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.end,
                                          children: [
                                            SizedBox(
                                              width: 20,
                                            ),
                                            if (!_selectedOptions.isEmpty)
                                              ..._returnWidget(),
                                          ],
                                        ),
                                      ],
                                    ),
                                  ),
                                  Container(
                                    padding: EdgeInsets.only(top: 7, bottom: 7),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        ElevatedButton(
                                          onPressed: _collapse,
                                          child: Container(
                                            padding: EdgeInsets.only(
                                                top: 10,
                                                bottom: 10,
                                                left: 135,
                                                right: 135),
                                            child: Text(
                                              'Info',
                                              style: TextStyle(fontSize: 20),
                                            ),
                                          ),
                                          style: ButtonStyle(
                                            backgroundColor:
                                                MaterialStatePropertyAll(
                                              Color.fromRGBO(13, 108, 114, 1),
                                            ),
                                            shape: MaterialStateProperty.all<
                                                RoundedRectangleBorder>(
                                              RoundedRectangleBorder(
                                                borderRadius:
                                                    BorderRadius.circular(20),
                                              ),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  if (_availableDatesForTrip != null &&
                                      _isCollapsed)
                                    Container(
                                      decoration: BoxDecoration(
                                          color: Color.fromRGBO(53, 56, 63, 1),
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(25))),
                                      padding: EdgeInsets.only(
                                          top: 15, left: 20, bottom: 15),
                                      margin:
                                          EdgeInsets.only(top: 10, bottom: 10),
                                      child: Column(
                                        children: [
                                          Row(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Container(
                                                padding: EdgeInsets.only(
                                                    right: 5, top: 2),
                                                child: Icon(
                                                  Icons
                                                      .access_time_filled_outlined,
                                                  color: Theme.of(context)
                                                      .accentColor,
                                                  size: 20,
                                                ),
                                              ),
                                              Text(
                                                'Duration:  ',
                                                style: TextStyle(
                                                    letterSpacing: 0.6,
                                                    fontSize: 21,
                                                    fontWeight: FontWeight.w500,
                                                    color: Theme.of(context)
                                                        .accentColor),
                                              ),
                                              Text(
                                                _totalDuration,
                                                style: TextStyle(
                                                    fontSize: 20,
                                                    fontWeight: FontWeight.w400,
                                                    color: Theme.of(context)
                                                        .accentColor),
                                              ),
                                            ],
                                          ),
                                          SizedBox(
                                            height: 15,
                                          ),
                                          Row(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Container(
                                                padding: EdgeInsets.only(
                                                    right: 5, top: 2),
                                                child: Icon(
                                                  Icons.directions_car_rounded,
                                                  color: Theme.of(context)
                                                      .accentColor,
                                                  size: 20,
                                                ),
                                              ),
                                              Text(
                                                'Distance:  ',
                                                style: TextStyle(
                                                    letterSpacing: 0.6,
                                                    fontSize: 21,
                                                    fontWeight: FontWeight.w500,
                                                    color: Theme.of(context)
                                                        .accentColor),
                                              ),
                                              Text(
                                                _totalDistance,
                                                style: TextStyle(
                                                    fontSize: 20,
                                                    fontWeight: FontWeight.w400,
                                                    color: Theme.of(context)
                                                        .accentColor),
                                              ),
                                            ],
                                          ),
                                          SizedBox(
                                            height: 15,
                                          ),
                                          Row(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Container(
                                                padding: EdgeInsets.only(
                                                    right: 5, top: 2),
                                                child: Icon(
                                                  Icons.date_range,
                                                  color: Theme.of(context)
                                                      .accentColor,
                                                  size: 20,
                                                ),
                                              ),
                                              Text(
                                                'Date Range:  ',
                                                style: TextStyle(
                                                    letterSpacing: 0.6,
                                                    fontSize: 21,
                                                    fontWeight: FontWeight.w500,
                                                    color: Theme.of(context)
                                                        .accentColor),
                                              ),
                                              Text(
                                                _totalDaysFormatted,
                                                style: TextStyle(
                                                    fontSize: 20,
                                                    fontWeight: FontWeight.w400,
                                                    color: Theme.of(context)
                                                        .accentColor),
                                              ),
                                            ],
                                          ),
                                        ],
                                      ),
                                    ),
                                  if (_availableDatesForTrip != null &&
                                      _isCollapsed)
                                    Container(
                                      margin: EdgeInsets.only(top: 10),
                                      padding:
                                          EdgeInsets.only(left: 5, top: 10),
                                      decoration: BoxDecoration(
                                          color: Color.fromRGBO(53, 56, 63, 1),
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(25))),
                                      child: Column(
                                        children: [
                                          Row(children: [
                                            Container(
                                              padding: EdgeInsets.only(
                                                  right: 3,
                                                  top: 4,
                                                  bottom: 7,
                                                  left: 15),
                                              child: Icon(
                                                Icons.event_available,
                                                color: Theme.of(context)
                                                    .accentColor,
                                                size: 24,
                                              ),
                                            ),
                                            Container(
                                              padding: EdgeInsets.only(
                                                  top: 5, left: 5, bottom: 10),
                                              child: Text(
                                                'Available Dates:',
                                                style: TextStyle(
                                                    letterSpacing: 0.6,
                                                    fontSize: 21,
                                                    fontWeight: FontWeight.w500,
                                                    color: Theme.of(context)
                                                        .accentColor),
                                              ),
                                            ),
                                          ]),
                                          for (var i = 0;
                                              i < _availableDatesForTrip.length;
                                              i++)
                                            Container(
                                              padding: const EdgeInsets.only(
                                                  top: 5, bottom: 15, left: 5),
                                              child: Row(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.center,
                                                children: [
                                                  Container(
                                                    padding: EdgeInsets.only(
                                                        right: 6, left: 12),
                                                    child: Icon(
                                                      Icons.check_rounded,
                                                      color: Theme.of(context)
                                                          .accentColor,
                                                      size: 20,
                                                    ),
                                                  ),
                                                  Text(
                                                    _availableDatesForTrip[i],
                                                    style: TextStyle(
                                                        letterSpacing: 0.6,
                                                        fontSize: 20,
                                                        fontWeight:
                                                            FontWeight.w400,
                                                        color: Theme.of(context)
                                                            .accentColor),
                                                  ),
                                                ],
                                              ),
                                            )
                                        ],
                                      ),
                                    ),
                                ],
                              ),
                      ],
                    );
                  },
                ),
              );
            },
          )
        ],
      ),
    );
  }
}
